#!/usr/bin/swift sh

import ArgumentParser // apple/swift-argument-parser == 0.0.4
import ShellKit // https://gitlab.com/thecb4/shellkit.git  == 2630153a
import Version // mxcl/Version == 2.0.0

let env = ["PATH": "/usr/local/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"]

extension Version: ExpressibleByArgument {}

extension ParsableCommand {
  static func run(using arguments: [String] = []) throws {
    let command = try parseAsRoot(arguments)
    try command.run()
  }
}

extension CommandConfiguration: ExpressibleByStringLiteral {
  public init(stringLiteral value: String) {
    self.init(abstract: value)
  }
}

extension Shell {
  public static var git_current_branch: String {
    let arguments = ["rev-parse", "--abbrev-ref", "HEAD"]

    guard let result = try? Shell.git(arguments: arguments, workingDirectory: Shell.Path.cwd, logLevel: .off) else {
      return ""
    }

    if result.status == 0 {
      return result.out
    } else {
      return ""
    }
  }
}

struct Calm: ParsableCommand {
  static var configuration = CommandConfiguration(
    abstract: "A utility for performing command line work",
    subcommands: [
      Test.self,
      Hygene.self,
      LocalIntegration.self,
      ContinuousIntegration.self,
      Save.self,
      Release.self,
      Documentation.self,
      Flow.self
    ],
    defaultSubcommand: Hygene.self
  )
}

extension Calm {
  struct Hygene: ParsableCommand {
    static var configuration: CommandConfiguration = "Perform hygene activities on the project"

    func run() throws {
      try ShellKit.validate(Shell.exists(at: "commit.yml"), "You need to add a commit.yml file")
      try ShellKit.validate(!Shell.git_ls_untracked.contains("commit.yml"), "You need to track commit file")
      try ShellKit.validate(Shell.git_ls_modified.contains("commit.yml"), "You need to update your commit file")
    }
  }

  struct Test: ParsableCommand {
    static var configuration: CommandConfiguration = "Run tests"

    func run() throws {
      try Shell.swiftTestGenerateLinuxMain(environment: env)
      try Shell.swiftFormat(version: "5.1", environment: env)

      var arguments = [
        "--parallel",
        "--xunit-output Tests/Results.xml",
        "--enable-code-coverage"
      ]

      #if os(Linux)
        arguments += ["--filter \"^(?!.*MacOS).*$\""]
      #endif

      try Shell.swiftTest(arguments: arguments)
    }
  }

  struct Save: ParsableCommand {
    static var configuration: CommandConfiguration = "git commit activities"

    func run() throws {
      try Hygene.run()
      try Shell.changelogger(arguments: ["log"])
      try Shell.git(arguments: ["add", "-A"])
      try Shell.git(arguments: ["commit", "-F", "commit.yml"])
    }
  }

  struct LocalIntegration: ParsableCommand {
    static var configuration = "Perform local integration"

    @Flag(help: "Save on integration completion")
    var save: Bool

    func run() throws {
      try Hygene.run()
      try Test.run()
      if save { try Save.run() }
    }
  }

  struct ContinuousIntegration: ParsableCommand {
    static var configuration: CommandConfiguration = "Perform continous integration"

    func run() throws {
      try Test.run()
    }
  }

  struct Documentation: ParsableCommand {
    static var configuration: CommandConfiguration = "Generate Documentation"

    func run() throws {
      try Shell.swiftDoc(
        name: "ShellKit",
        output: "docs",
        author: "Cavelle Benjamin",
        authorUrl: "https://thecb4.io",
        twitterHandle: "_thecb4",
        gitRepository: "https://github.com/thecb4/ShellKit"
      )
    }
  }
}

extension Calm {
  struct Release: ParsableCommand {
    static var configuration = CommandConfiguration(
      abstract: "Release of work",
      subcommands: [
        New.self,
        Prepare.self,
        Publish.self
      ],
      defaultSubcommand: New.self
    )
  }
}

extension Calm.Release {
  struct New: ParsableCommand {
    static var configuration = "creates new release (tag)"
    // TO-DO: move to an option group
    @Argument(help: "version for the release")
    var version: Version

    func run() {
      print("new release \(version)")
    }
  }

  struct Prepare: ParsableCommand {
    static var configuration: CommandConfiguration = "prepare the current release"

    @Argument(help: "summary of the release to prepare")
    var summary: String

    @Argument(help: "version for the release")
    var version: Version

    func run() throws {
      let files = try Shell.git(arguments: ["status", "--untracked-files=no", "--porcelain"])
      try ShellKit.validate(files.out == "", "Dirt repository. Clean it up before preparing your release")

      try Shell.changelogger(arguments: ["release", "\"\(summary)\"", "--version-tag", version.description], environment: env)
      try Shell.changelogger(arguments: ["markdown"])

      try Shell.git(arguments: ["add", "-A"])
      try Shell.git(arguments: ["commit", "-F", "commit.yml"])
    }
  }

  struct Publish: ParsableCommand {
    @Argument(help: "version for the release")
    var version: Version

    func run() {
      print("new release \(version)")
    }
  }
}

extension Calm {
  struct Flow: ParsableCommand {
    static var configuration = CommandConfiguration(
      abstract: "Git flow commands",
      subcommands: [
        Init.self,
        Feature.self,
        Release.self
      ],
      defaultSubcommand: Init.self
    )

    static var remote: String?
  }
}

extension Calm.Flow {
  struct Init: ParsableCommand {
    static var configuration: CommandConfiguration = "Initialize flow"

    func run() throws {
      print("flow init")
      try Shell.git(arguments: ["init"])
      try Shell.git(arguments: ["commit", "--allow-empty", "-m", "\"Initial commit\""])
      try Shell.git(arguments: ["checkout", "-b", "develop", "master"])
      if let remote = Calm.Flow.remote {
        try Shell.git(arguments: ["remote", "add", "origin", remote])
      }
    }
  }

  struct Feature: ParsableCommand {
    static var configuration = CommandConfiguration(
      abstract: "Manage git flow features",
      subcommands: [
        Start.self,
        Publish.self,
        Pull.self,
        Finish.self
      ],
      defaultSubcommand: Start.self
    )

    static var `prefix`: String = "feature"
  }

  struct Release: ParsableCommand {
    static var configuration = CommandConfiguration(
      abstract: "Manage git flow releases",
      subcommands: [
        Start.self,
        Publish.self,
        // Pull.self,
        Finish.self
      ],
      defaultSubcommand: Start.self
    )

    static var `prefix`: String = "release"

  }
}

extension Calm.Flow.Feature {
  struct Start: ParsableCommand {
    static var configuration: CommandConfiguration = "Create a feature branch"

      @Argument(help: "Feature name")
      var name: String

    func run() throws {
      try ShellKit.validate(Shell.git_current_branch != "master", "cannot create feature branch from master")
      print("starting a feature named \(name)")
      try Shell.git(arguments: ["checkout", "-b", "\(Calm.Flow.Feature.prefix)/\(name)", "develop"])
    }
  }

  struct Pull: ParsableCommand {

    static var configuration = "Get latest for a feature branch"

    @Argument(
    default: Shell.git_current_branch.replacingOccurrences(of: "\(Calm.Flow.Feature.prefix)/", with: ""),
      help: "Feature name"
    )
    var name: String

    func run() throws {
      print("pull latest for feature: \(name) from origin")
      //try Shell.git(arguments: ["checkout", "feature/MYFEATURE"])
      //try Shell.git(arguments: ["pull", "--rebase", "origin", "feature/MYFEATURE"])
    }
  }

  struct Finish: ParsableCommand {
    static var configuration: CommandConfiguration = "Finalize a feature branch"

    @Argument(
      default: Shell.git_current_branch.replacingOccurrences(of: "\(Calm.Flow.Feature.prefix)/", with: ""),
        help: "Feature name"
    )
    var name: String

    func run() throws {
      print("finalize feature: \(name)")
      try Shell.git(arguments: ["checkout", "develop"])
      try Shell.git(arguments: ["merge", "--no-ff", "\(Calm.Flow.Feature.prefix)/\(name)"])
      try Shell.git(arguments: ["branch", "-d", "\(Calm.Flow.Feature.prefix)/\(name)"])
    }
  }

  struct Publish: ParsableCommand {
    static var configuration: CommandConfiguration = "Publish a feature branch"

    @Argument(
    default: Shell.git_current_branch.replacingOccurrences(of: "\(Calm.Flow.Feature.prefix)/", with: ""),
      help: "Feature name"
    )
    var name: String

    func run() throws {
      print("publish feature: \(name)")
      try Shell.git(arguments: ["checkout", "\(Calm.Flow.Feature.prefix)/\(name)"])
      try Shell.git(arguments: ["push", "origin", "\(Calm.Flow.Feature.prefix)/\(name)"])
    }
  }
}

extension Calm.Flow.Release {
  struct Start: ParsableCommand {
    static var configuration = "Create a release branch"

    @Argument(
    default: Version(Shell.git_current_branch.replacingOccurrences(of: "\(Calm.Flow.Release.prefix)/", with: "")),
        help: "Release version"
    )
    var version: Version

    func run() throws {
      print("starting release: \(version)")
      try Shell.git(arguments: ["checkout", "-b", "\(Calm.Flow.Release.prefix)/\(version)", "develop"])
    }
  }

  // struct Pull: ParsableCommand {
  //   static var configuration = "Get latest for a release branch"
  //
  //   func run() throws {
  //     print("pull latest for a release from origin")
  //     //try Shell.git(arguments: ["checkout", "release/version"])
  //     //try Shell.git(arguments: ["pull", "--rebase", "origin", "release/version"])
  //   }
  // }

  struct Finish: ParsableCommand {
    static var configuration = "Finalize a release branch"

    @Argument(
    default: Version(Shell.git_current_branch.replacingOccurrences(of: "\(Calm.Flow.Release.prefix)/", with: "")),
        help: "Release version"
    )
    var version: Version

    func run() throws {
      print("finalize release: \(version)")
      try Shell.git(arguments: ["checkout", "master"])
      try Shell.git(arguments: ["merge", "--no-ff", "\(Calm.Flow.Release.prefix)/\(version)"])
      try Shell.git(arguments: ["tag", "-a", "\(version)"])
      try Shell.git(arguments: ["checkout", "develop"])
      try Shell.git(arguments: ["merge", "--no-ff", "\(Calm.Flow.Release.prefix)/\(version)"])
      try Shell.git(arguments: ["branch", "-d", "\(Calm.Flow.Release.prefix)/\(version)"])
    }
  }

  struct Publish: ParsableCommand {
    static var configuration = "Publish a release branch"

    @Argument(
    default: Version(Shell.git_current_branch.replacingOccurrences(of: "\(Calm.Flow.Release.prefix)/", with: "")),
        help: "Release version"
    )
    var version: Version

    func run() throws {
      print("publish release: \(version)")
      try Shell.git(arguments: ["checkout", "develop"])
      try Shell.git(arguments: ["push", "origin", "develop"])
      try Shell.git(arguments: ["push", "origin", "\(version)"])
      try Shell.git(arguments: ["push", "origin", "master"])
    }
  }
}

Calm.Flow.remote = "git@gitlab.com:thecb4/calm-test.git"

Calm.main()
